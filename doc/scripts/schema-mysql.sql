DROP TABLE IF EXISTS entrevista;

CREATE TABLE entrevistas.entrevista (
    id BIGINT NOT NULL AUTO_INCREMENT ,
    entrevistador VARCHAR(45) NOT NULL ,
    entrevistado VARCHAR(45) NOT NULL ,
    fecha TIMESTAMP NOT NULL ,
    comentario VARCHAR(255) NULL ,
    realizada BOOLEAN NOT NULL ,
    PRIMARY KEY (id)
);

INSERT INTO entrevista VALUES (1, 'Juan Bori', 'Pablo', STR_TO_DATE('13/08/2019 14:00', '%d/%m/%Y %H:%i'), 'Este es un comentario', TRUE);
INSERT INTO entrevista VALUES (2, 'Juan Bori', 'Ezequiel', STR_TO_DATE('22/08/2019 16:00', '%d/%m/%Y %H:%i'), NULL, FALSE);
INSERT INTO entrevista VALUES (3, 'Juan Bori', 'Alan', STR_TO_DATE('23/08/2019 14:00', '%d/%m/%Y %H:%i'), 'Este es un comentario v2', FALSE);
INSERT INTO entrevista VALUES (4, 'Juan Bori', 'Rober', STR_TO_DATE('24/08/2019 14:00', '%d/%m/%Y %H:%i'), NULL, FALSE);

INSERT INTO entrevista VALUES (5, 'Juanma', 'Pablo', STR_TO_DATE('14/08/2019 11:00', '%d/%m/%Y %H:%i'), 'Este es un comentario', TRUE);
INSERT INTO entrevista VALUES (6, 'Juanma', 'Ezequiel', STR_TO_DATE('20/08/2019 17:00', '%d/%m/%Y %H:%i'), NULL, FALSE);
INSERT INTO entrevista VALUES (7, 'Juanma', 'Alan', STR_TO_DATE('21/08/2019 13:00', '%d/%m/%Y %H:%i'), 'Este es un comentario v2', FALSE);
INSERT INTO entrevista VALUES (8, 'Juanma', 'Rober', STR_TO_DATE('27/08/2019 14:30', '%d/%m/%Y %H:%i'), NULL, FALSE);