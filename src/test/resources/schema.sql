DROP TABLE IF EXISTS entrevista;

CREATE TABLE entrevista (
    id BIGINT IDENTITY PRIMARY KEY,
    entrevistador VARCHAR(45) NOT NULL,
    entrevistado VARCHAR(45) NOT NULL,
    fecha DATETIME NOT NULL,
    comentario VARCHAR(255),
    realizada BOOLEAN NOT NULL
);

INSERT INTO entrevista VALUES (1, 'Juan Bori', 'Pablo', PARSEDATETIME('13/08/2019 14:00', 'dd/MM/yyyy HH:mm'), 'Este es un comentario', TRUE);
INSERT INTO entrevista VALUES (2, 'Juan Bori', 'Ezequiel', PARSEDATETIME('22/08/2019 16:00', 'dd/MM/yyyy HH:mm'), NULL, FALSE);
INSERT INTO entrevista VALUES (3, 'Juan Bori', 'Alan', PARSEDATETIME('23/08/2019 14:00', 'dd/MM/yyyy HH:mm'), 'Este es un comentario v2', FALSE);
INSERT INTO entrevista VALUES (4, 'Juan Bori', 'Rober', PARSEDATETIME('24/08/2019 14:00', 'dd/MM/yyyy HH:mm'), NULL, FALSE);

INSERT INTO entrevista VALUES (5, 'Juanma', 'Pablo', PARSEDATETIME('14/08/2019 11:00', 'dd/MM/yyyy HH:mm'), 'Este es un comentario', TRUE);
INSERT INTO entrevista VALUES (6, 'Juanma', 'Ezequiel', PARSEDATETIME('20/08/2019 17:00', 'dd/MM/yyyy HH:mm'), NULL, FALSE);
INSERT INTO entrevista VALUES (7, 'Juanma', 'Alan', PARSEDATETIME('21/08/2019 13:00', 'dd/MM/yyyy HH:mm'), 'Este es un comentario v2', FALSE);
INSERT INTO entrevista VALUES (8, 'Juanma', 'Rober', PARSEDATETIME('27/08/2019 14:30', 'dd/MM/yyyy HH:mm'), NULL, FALSE);