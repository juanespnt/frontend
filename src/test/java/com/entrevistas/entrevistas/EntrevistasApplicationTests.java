package com.entrevistas.entrevistas;

import com.entrevistas.entrevistas.domain.Entrevista;
import com.entrevistas.entrevistas.service.EntrevistaService;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class EntrevistasApplicationTests {

    @Autowired
    private EntrevistaService entrevistaService;

    @Test
    public void buscarTodas_existenEntrevistas_devuelveEntrevistas() {
        List<Entrevista> entrevistas = entrevistaService.buscarTodas();

        assertThat(entrevistas)
                .asList()
                .isNotEmpty();
    }

    @Test
    public void guardar_conEntrevistaValida_guardadoSatisfactorio() {

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm");
        LocalDateTime fecha = LocalDateTime.parse("20/08/2019 11:00", formatter);
        Entrevista entrevista = new Entrevista("Juan", "Juanma", fecha, "", false);

        Entrevista entrevistaGuardada = entrevistaService.guardar(entrevista);

        assertThat(entrevistaGuardada)
                .extracting("entrevistador").isEqualTo(entrevista.getEntrevistador());
        assertThat(entrevistaGuardada)
                .extracting("entrevistado").isEqualTo(entrevista.getEntrevistado());
        assertThat(entrevistaGuardada)
                .extracting("fecha").isEqualTo(entrevista.getFecha());
        assertThat(entrevistaGuardada)
                .extracting("comentario").isEqualTo(entrevista.getComentario());
        assertThat(entrevistaGuardada)
                .extracting("realizada").isEqualTo(entrevista.getRealizada());
        assertThat(entrevistaGuardada)
                .extracting("id").isNotNull();
        assertThat(entrevistaGuardada.getId() > 0).isTrue();
    }

    @Test
    public void guardar_conEntrevistaNula_lanzaExcepcion() {
        Entrevista entrevista = null;

        assertThatExceptionOfType(java.lang.IllegalArgumentException.class)
                .isThrownBy(() -> entrevistaService.guardar(entrevista))
                .withMessage("La entrevista no puede ser nula");
    }

    @Test
    public void guardar_conEntrevistadorNulo_lanzaExcepcion() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm");
        LocalDateTime fecha = LocalDateTime.parse("20/08/2019 11:00", formatter);
        Entrevista entrevista = new Entrevista(null, "Juanma", fecha, "", false);

        assertThatExceptionOfType(java.lang.IllegalArgumentException.class)
                .isThrownBy(() -> entrevistaService.guardar(entrevista))
                .withMessage("El entrevistador no puede ser nulo");
    }

    @Test
    public void guardar_conEntrevistadoNulo_lanzaExcepcion() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm");
        LocalDateTime fecha = LocalDateTime.parse("20/08/2019 11:00", formatter);
        Entrevista entrevista = new Entrevista("Juan", null, fecha, "", false);

        assertThatExceptionOfType(java.lang.IllegalArgumentException.class)
                .isThrownBy(() -> entrevistaService.guardar(entrevista))
                .withMessage("El entrevistado no puede ser nulo");
    }

    @Test
    public void guardar_conFechaNula_lanzaExcepcion() {
        Entrevista entrevista = new Entrevista("Juan", "juanma", null, "", false);

        assertThatExceptionOfType(java.lang.IllegalArgumentException.class)
                .isThrownBy(() -> entrevistaService.guardar(entrevista))
                .withMessage("La fecha no puede ser nula");
    }

    @Test
    public void guardar_conRealizadaNulo_lanzaExcepcion() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm");
        LocalDateTime fecha = LocalDateTime.parse("20/08/2019 11:00", formatter);
        Entrevista entrevista = new Entrevista("Juan", "juanma", fecha, "", null);

        assertThatExceptionOfType(java.lang.IllegalArgumentException.class)
                .isThrownBy(() -> entrevistaService.guardar(entrevista))
                .withMessage("El estado de la entrevista no puede ser nulo");
    }

    @Test
    public void guardar_conEntrevistadorVacio_lanzaExcepcion() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm");
        LocalDateTime fecha = LocalDateTime.parse("20/08/2019 11:00", formatter);
        Entrevista entrevista = new Entrevista("", "Juanma", fecha, "", false);

        assertThatExceptionOfType(java.lang.IllegalArgumentException.class)
                .isThrownBy(() -> entrevistaService.guardar(entrevista))
                .withMessage("La longitud del nombre del entrevistador no puede ser 0");
    }

    @Test
    public void guardar_conEntrevistadoVacio_lanzaExcepcion() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm");
        LocalDateTime fecha = LocalDateTime.parse("20/08/2019 11:00", formatter);
        Entrevista entrevista = new Entrevista("Juan", "", fecha, "", false);

        assertThatExceptionOfType(java.lang.IllegalArgumentException.class)
                .isThrownBy(() -> entrevistaService.guardar(entrevista))
                .withMessage("La longitud del nombre del entrevistado no puede ser 0");
    }

    @Test
    public void guardar_conComentariosCaracteresExcedidos_lanzaExcepcion() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm");
        LocalDateTime fecha = LocalDateTime.parse("20/08/2019 11:00", formatter);
        String comentario = "a";

        for (int i = 0; i < 9999; i++) {
            comentario += "a";
        }

        Entrevista entrevista = new Entrevista("Juan", "juanma", fecha, comentario, false);

        assertThatExceptionOfType(IllegalArgumentException.class)
                .isThrownBy(() -> entrevistaService.guardar(entrevista))
                .withMessage("La longitud del comentario no puede exceder los 255 caracteres");
    }

    @Test
    public void guardar_conEntrevistadorCaracteresExcedidos_lanzaExcepcion() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm");
        LocalDateTime fecha = LocalDateTime.parse("20/08/2019 11:00", formatter);
        String entrevistador = "a";

        for (int i = 0; i < 9999; i++) {
            entrevistador += "a";
        }

        Entrevista entrevista = new Entrevista(entrevistador, "", fecha, "", false);

        assertThatExceptionOfType(java.lang.IllegalArgumentException.class)
                .isThrownBy(() -> entrevistaService.guardar(entrevista))
                .withMessage("La longitud del nombre del entrevistador no puede superar los 45 caracteres");
    }

    @Test
    public void guardar_conEntrevistadoCaracteresExcedidos_lanzaExcepcion() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm");
        LocalDateTime fecha = LocalDateTime.parse("20/08/2019 11:00", formatter);
        String entrevistado = "a";

        for (int i = 0; i < 9999; i++) {
            entrevistado += "a";
        }

        Entrevista entrevista = new Entrevista("Juan", entrevistado, fecha, "", false);

        assertThatExceptionOfType(java.lang.IllegalArgumentException.class)
                .isThrownBy(() -> entrevistaService.guardar(entrevista))
                .withMessage("La longitud del nombre del entrevistado no puede superar los 45 caracteres");
    }

}
