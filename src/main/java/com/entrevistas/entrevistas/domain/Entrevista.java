package com.entrevistas.entrevistas.domain;

import java.io.Serializable;
import java.time.LocalDateTime;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
public class Entrevista implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String entrevistador;
    private String entrevistado;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private LocalDateTime fecha;

    private String comentario;
    private Boolean realizada;

    public Entrevista(String entrevistador, String entrevistado, LocalDateTime fecha, String comentario, Boolean realizada) {
        this.entrevistador = entrevistador;
        this.entrevistado = entrevistado;
        this.fecha = fecha;
        this.comentario = comentario;
        this.realizada = realizada;
    }

    public Entrevista() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEntrevistador() {
        return entrevistador;
    }

    public void setEntrevistador(String entrevistador) {
        this.entrevistador = entrevistador;
    }

    public String getEntrevistado() {
        return entrevistado;
    }

    public void setEntrevistado(String entrevistado) {
        this.entrevistado = entrevistado;
    }

    public LocalDateTime getFecha() {
        return fecha;
    }

    public void setFecha(LocalDateTime fecha) {
        this.fecha = fecha;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public Boolean getRealizada() {
        return realizada;
    }

    public void setRealizada(Boolean realizada) {
        this.realizada = realizada;
    }

    @Override
    public String toString() {
        return "Entrevista{" + "id=" + id + ", entrevistador=" + entrevistador + ", entrevistado=" + entrevistado + ", fecha=" + fecha + ", comentario=" + comentario + ", realizada=" + realizada + '}';
    }

}
