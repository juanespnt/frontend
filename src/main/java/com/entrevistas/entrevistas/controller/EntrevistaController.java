package com.entrevistas.entrevistas.controller;

import com.entrevistas.entrevistas.domain.Entrevista;
import com.entrevistas.entrevistas.service.EntrevistaService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class EntrevistaController {

    @Autowired
    private EntrevistaService entrevistaService;

    @GetMapping("/")
    public String main(Model model) {
        List<Entrevista> entrevistas = entrevistaService.buscarTodas();

        model.addAttribute("entrevistas", entrevistas);
        model.addAttribute("entrevista", new Entrevista());

        return "index";
    }

    @PostMapping("/")
    public String guardarEntrevista(@ModelAttribute(value = "entrevista") Entrevista entrevista) {

        entrevistaService.guardar(entrevista);
        return "redirect:/";
    }
}