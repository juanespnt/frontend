package com.entrevistas.entrevistas.service;

import com.entrevistas.entrevistas.domain.Entrevista;
import java.util.List;

public interface EntrevistaService {

    List<Entrevista> buscarTodas();

    public Entrevista guardar(Entrevista entrevista);
}
