package com.entrevistas.entrevistas.service.impl;

import com.entrevistas.entrevistas.domain.Entrevista;
import com.entrevistas.entrevistas.repository.EntrevistaRepository;
import com.entrevistas.entrevistas.service.EntrevistaService;
import java.time.LocalDateTime;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

@Service
@Transactional
public class EntrevistaServiceImpl implements EntrevistaService {

    private final EntrevistaRepository entrevistaRepository;

    public EntrevistaServiceImpl(EntrevistaRepository entrevistaRepository) {
        this.entrevistaRepository = entrevistaRepository;
    }

    @Override
    public List<Entrevista> buscarTodas() {
        return entrevistaRepository.findAll();
    }

    @Override
    public Entrevista guardar(Entrevista entrevista) {
        validar(entrevista);
        return entrevistaRepository.save(entrevista);
    }

    private void validar(Entrevista entrevista) {
        Assert.notNull(entrevista, "La entrevista no puede ser nula");
        validarEntrevistador(entrevista.getEntrevistador());
        validarEntrevistado(entrevista.getEntrevistado());
        validarFecha(entrevista.getFecha());
        validarRealizada(entrevista.getRealizada());
        validarComentario(entrevista.getComentario());

    }

    private void validarEntrevistador(String entrevistador) {
        Assert.notNull(entrevistador, "El entrevistador no puede ser nulo");
        Assert.isTrue(entrevistador.length() < 45, "La longitud del nombre del entrevistador no puede superar los 45 caracteres");
        Assert.hasLength(entrevistador, "La longitud del nombre del entrevistador no puede ser 0");
    }

    private void validarEntrevistado(String entrevistado) {
        Assert.notNull(entrevistado, "El entrevistado no puede ser nulo");
        Assert.isTrue(entrevistado.length() < 45, "La longitud del nombre del entrevistado no puede superar los 45 caracteres");
        Assert.hasLength(entrevistado, "La longitud del nombre del entrevistado no puede ser 0");
    }

    private void validarFecha(LocalDateTime fecha) {
        Assert.notNull(fecha, "La fecha no puede ser nula");
    }

    private void validarRealizada(Boolean realizada) {
        Assert.notNull(realizada, "El estado de la entrevista no puede ser nulo");
    }

    private void validarComentario(String comentario) {
        Assert.isTrue(comentario.length() < 255, "La longitud del comentario no puede exceder los 255 caracteres");
    }

}
