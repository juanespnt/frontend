package com.entrevistas.entrevistas.repository;

import com.entrevistas.entrevistas.domain.Entrevista;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EntrevistaRepository extends JpaRepository<Entrevista, Long> {

}
